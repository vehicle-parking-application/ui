import {Component, OnInit} from '@angular/core';
import {CurrentReservationService} from "../../_services/current-reservation/current-reservation.service";
import {ParkingReservation} from "../../models/parking-reservation";
import {StorageService} from "../../_services/storage.service";
import {ComponentInteractionService} from "../../_services/component-interaction.service";
import {HotToastService} from "@ngneat/hot-toast";

@Component({
  selector: 'app-current-reservation',
  templateUrl: './current-reservation.component.html',
  styleUrls: ['./current-reservation.component.css']
})
export class CurrentReservationComponent implements OnInit {
  currentReservation: ParkingReservation;
  isCurrentReservation: boolean;
  parkingSlotType: string;

  constructor(private currentReservationService: CurrentReservationService,
              private storageService: StorageService,
              private componentInteractionService: ComponentInteractionService,
              private hotToastService: HotToastService) {
  }

  ngOnInit(): void {
    this.getCurrentReservation();
    this.isCurrentReservationPresent();
    this.componentInteractionService.getRefreshCurrentReservationObservable().subscribe(() => {
      this.getCurrentReservation();
      this.isCurrentReservationPresent()
    });
  }

  getCurrentReservation() {
    this.currentReservationService.getCurrentReservation(this.storageService.getUser().id)
      .subscribe(
        response => {
          this.currentReservation = response;
          this.parkingSlotType = response.parkingSlot.type;
        },
        error => {
          console.error('Error fetching current reservation:', error);
        }
      );
  }

  isCurrentReservationPresent() {
    this.currentReservationService.isCurrentReservationPresent(this.storageService.getUser().id)
      .subscribe(
        response => {
          this.isCurrentReservation = response;
        },
        error => {
          console.error('Error fetching current reservation:', error);
        }
      );
  }

  addOneHour() {
    this.displayLoadingMessage("addOneHourLoading");
    this.currentReservationService.addOneHour(this.currentReservation)
      .subscribe(
        response => {
          this.currentReservation = response;
          this.hotToastService.close("addOneHourLoading");
          this.hotToastService.success("One Hour Extended")
        },
        error => {
          this.hotToastService.close("addOneHourLoading");
          this.hotToastService.error(error.error)
          console.error('Error adding 1 hour:', error);
        }
      );
  }

  leave() {
    if (confirm("Are you sure you want to leave?")) {
      this.displayLoadingMessage("leaveLoading");
      this.currentReservationService.leave(this.currentReservation)
        .subscribe(
          response => {
            this.isCurrentReservation = false;
            this.hotToastService.close("leaveLoading");
            this.hotToastService.success(response.message);
            this.componentInteractionService.refreshParkingHistoryComponent();
          },
          error => {
            this.hotToastService.close("leaveLoading");
            this.hotToastService.error(error.error)
            console.error('Error marking as leave:', error);
          }
        );
    }
  }

  cancel() {
    if (confirm("Are you sure you want to cancel?")) {
      this.displayLoadingMessage("cancelLoading");
      this.currentReservationService.cancelReservation(this.currentReservation)
        .subscribe(
          response => {
            this.isCurrentReservation = false;
            this.hotToastService.close("cancelLoading");
            this.hotToastService.success(response.message);
            this.componentInteractionService.refreshParkingHistoryComponent();
          },
          error => {
            this.hotToastService.close("cancelLoading");
            this.hotToastService.error(error.error)
            console.error('Error cancelling reservation:', error);
          }
        );
    }
  }

  protected readonly Date = Date;

  displayLoadingMessage(id: string){
    this.hotToastService.loading("Loading", {
      id: id
    })
  }
}
