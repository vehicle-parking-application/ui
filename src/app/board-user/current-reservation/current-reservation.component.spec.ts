import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentReservationComponent } from './current-reservation.component';

describe('CurrentReservationComponent', () => {
  let component: CurrentReservationComponent;
  let fixture: ComponentFixture<CurrentReservationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CurrentReservationComponent]
    });
    fixture = TestBed.createComponent(CurrentReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
