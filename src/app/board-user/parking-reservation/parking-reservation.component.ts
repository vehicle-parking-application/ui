import {Component, OnInit} from '@angular/core';
import {UserService} from "../../_services/user.service";
import {StorageService} from "../../_services/storage.service";
import {ParkingReservationService} from "../../_services/parking-reservation/parking-reservation.service";
import {UtilsService} from "../../_services/utils.service";
import {HotToastService} from "@ngneat/hot-toast";
import {ComponentInteractionService} from "../../_services/component-interaction.service";

@Component({
  selector: 'app-parking-reservation',
  templateUrl: './parking-reservation.component.html',
  styleUrls: ['./parking-reservation.component.css']
})
export class ParkingReservationComponent implements OnInit {
  content?: string;
  fromTime: string;
  toTime: string;
  currentUser: any;
  isCurrentReservationAvailable: boolean;

  constructor(private userService: UserService, private storageService: StorageService,
              private parkingReservationService: ParkingReservationService,
              private hotToastService: HotToastService,
              private componentInteractionService: ComponentInteractionService) {
  }

  ngOnInit(): void {
    this.reservationFormTimePicker();
    this.currentUser = this.storageService.getUser();
  }

  reservationFormTimePicker() {
    this.currentUser = this.storageService.getUser();
    const currentTime = new Date();
    currentTime.setHours(currentTime.getHours() + 1);
    this.fromTime = this.formatTime(currentTime);
    currentTime.setHours(currentTime.getHours() + 3);
    this.toTime = this.formatTime(currentTime);
  }

  formatTime(date: Date): string {
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const formattedHours = hours < 10 ? '0' + hours : hours.toString();
    const formattedMinutes = minutes < 10 ? '0' + minutes : minutes.toString();
    return formattedHours + ':' + formattedMinutes;
  }

  reserveSlot() {
    this.hotToastService.loading("Loading", {
      id: "reserveSlotLoading"
    })
    const data = {
      userId: this.currentUser.id,
      mode: UtilsService.findRadioValue("mode"),
      fromTime: UtilsService.convertTimeToDate(this.fromTime),
      toTime: UtilsService.convertTimeToDate(this.toTime)
    };
    this.parkingReservationService.reserveParkingSlot(data).subscribe(
      response => {
        this.hotToastService.close("reserveSlotLoading")
        this.hotToastService.success(response.message);
        this.componentInteractionService.refreshCurrentReservationComponent();
        console.log('Reservation successful:', response);
      },
      error => {
        this.hotToastService.close("reserveSlotLoading")
        this.hotToastService.error(error.error)
        console.error('Error:', error);
      }
    );
  }
}
