import {Component, OnInit} from '@angular/core';
import {ParkingHistoryService} from "../../_services/parking-history/parking-history.service";
import {StorageService} from "../../_services/storage.service";
import {ParkingReservation} from "../../models/parking-reservation";
import {ComponentInteractionService} from "../../_services/component-interaction.service";

@Component({
  selector: 'app-parking-history',
  templateUrl: './parking-history.component.html',
  styleUrls: ['./parking-history.component.css']
})
export class ParkingHistoryComponent implements OnInit {
  parkingHistoryList: ParkingReservation[];
  constructor(private parkingHistoryService: ParkingHistoryService,
              private storageService: StorageService,
              private componentInteractionService: ComponentInteractionService,) {
  }
  ngOnInit(): void {
    this.getBriefHistory();
    this.componentInteractionService.getRefreshParkingHistoryObservable().subscribe(() => {
      this.getBriefHistory();
    })
  }

  getBriefHistory(){
    this.parkingHistoryService.getBriefHistory(this.storageService.getUser().id)
      .subscribe(
        response => {
          this.parkingHistoryList = response;
        },
        error => {
          console.error('Error fetching parking reservation history:', error);
        }
      );
  }

}
