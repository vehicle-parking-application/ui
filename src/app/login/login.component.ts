import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { StorageService } from '../_services/storage.service';
import { HotToastService } from '@ngneat/hot-toast';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  user = '';

  constructor(private authService: AuthService, private storageService: StorageService,
              private hotToastService: HotToastService, private router: Router) { }

  ngOnInit(): void {
    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.user = this.storageService.getUser().username;
    }
  }

  onSubmit(): void {
    const { username, password } = this.form;
    this.hotToastService.loading("Signing in", {
      id: "signInLoading"
    })
    this.authService.login(username, password).subscribe({
      next: data => {
        this.storageService.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.storageService.getUser().roles;
        this.reloadPage();
      },
      error: err => {
        this.errorMessage = err.message;
        this.isLoginFailed = true;
      }
    });
    this.hotToastService.close("signInLoading");
  }

  reloadPage(): void {
    this.router.navigateByUrl('/home').then(r => window.location.reload());
  }
}
