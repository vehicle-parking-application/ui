import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {
  private sharedValueSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  sharedValue$: Observable<any> = this.sharedValueSubject.asObservable();

  constructor() { }

  setSharedValue(value: any): void {
    this.sharedValueSubject.next(value);
  }

  getSharedValue(): Observable<any> {
    return this.sharedValue$;
  }

}
