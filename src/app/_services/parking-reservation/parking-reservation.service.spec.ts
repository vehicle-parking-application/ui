import { TestBed } from '@angular/core/testing';

import { ParkingReservationService } from './parking-reservation.service';

describe('ParkingReservationService', () => {
  let service: ParkingReservationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParkingReservationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
