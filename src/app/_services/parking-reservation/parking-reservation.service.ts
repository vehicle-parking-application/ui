import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ParkingReservationService {

  private apiUrl = environment.apiUrl + '/parking/reserve';

  constructor(private http: HttpClient) {
  }

  reserveParkingSlot(data: any): Observable<any> {
    return this.http.post<any>(this.apiUrl, data);
  }
}
