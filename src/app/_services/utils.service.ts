import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  static convertTimeToDate(timeValue: any): Date {
    const [hours, minutes] = timeValue.split(':').map(Number);
    const date = new Date();
    date.setHours(hours);
    date.setMinutes(minutes);
    return date;
  }

  static findRadioValue(elementName: any){
    const radios = document.getElementsByName(elementName);
    let selectedValue = "";
    for (let i = 0; i < radios.length; i++) {
      const radio = radios[i] as HTMLInputElement;
      if (radio.checked) {
        selectedValue = radio.value;
        break;
      }
    }
    return selectedValue;
  }

}
