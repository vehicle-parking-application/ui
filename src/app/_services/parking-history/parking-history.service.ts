import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ParkingHistoryService {
  private apiUrl = environment.apiUrl + '/parking/history';

  constructor(private http: HttpClient) { }


  getBriefHistory(userId: number): Observable<any> {
    return this.http.get<any>(this.apiUrl + `/get-brief-history?userId=${userId}`);
  }
}
