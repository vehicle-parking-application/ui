import { TestBed } from '@angular/core/testing';

import { CurrentReservationService } from './current-reservation.service';

describe('CurrentReservationService', () => {
  let service: CurrentReservationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CurrentReservationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
