import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ParkingReservation} from "../../models/parking-reservation";

@Injectable({
  providedIn: 'root'
})
export class CurrentReservationService {
  private apiUrl = environment.apiUrl + '/parking';

  constructor(private http: HttpClient) {
  }

  getCurrentReservation(userId: number): Observable<any> {
    return this.http.get<any>(this.apiUrl + `/current-reservation?userId=${userId}`);
  }

  isCurrentReservationPresent(userId: number): Observable<boolean> {
    return this.http.get<any>(this.apiUrl + `/is-current-reservation-present?userId=${userId}`);
  }

  addOneHour(currentReservation: ParkingReservation): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/add-one-hour`, currentReservation);
  }

  leave(currentReservation: ParkingReservation): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/leave`, currentReservation);
  }

  cancelReservation(currentReservation: ParkingReservation): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/cancel`, currentReservation);
  }

}
