import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ComponentInteractionService {
  private refreshCurrentReservation = new Subject<void>();

  constructor() { }

  refreshCurrentReservationComponent() {
    this.refreshCurrentReservation.next();
  }
  refreshParkingHistoryComponent() {
    this.refreshCurrentReservation.next();
  }

  getRefreshCurrentReservationObservable() {
    return this.refreshCurrentReservation.asObservable();
  }

  getRefreshParkingHistoryObservable() {
    return this.refreshCurrentReservation.asObservable();
  }
}
