import {User} from "./user";
import {ParkingSlot} from "./parking-slot";


export class ParkingReservation {
  id: number;
  user: User;
  parkingSlot: ParkingSlot;
  fromTime: Date;
  toTime: Date;
  timestamp: Date;
  status: string;
}
