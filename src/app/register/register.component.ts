import { Component } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { HotToastService } from '@ngneat/hot-toast';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  form: any = {
    username: null,
    email: null,
    password: null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService, private hotToastService: HotToastService) { }

  onSubmit(): void {
    const { username, email, password } = this.form;
    this.hotToastService.loading("Signing up", {
      id: "signUpLoading"
    })
    this.authService.register(username, email, password).subscribe({
      next: data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      error: err => {
        this.errorMessage = err.message;
        this.isSignUpFailed = true;
      }
    });
    this.hotToastService.close("signUpLoading");
  }
}
